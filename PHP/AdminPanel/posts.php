<?php
    require_once 'header.php';

    if (isset($_SESSION['adminStatus']) == 0) {


        $do = isset($_GET['do']) ? $_GET['do'] : 'Manage';

?>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="left-div">
                <div class="user-settings-wrapper">
                    <ul class="nav">
                       <li style="text-align: left; float: left; font-size: 32px; margin-top: 15px;">Socail Media </li>
                        <li>
                            <a href="#">
                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                            </a>
                        </li>
                       <li>
                            <a href="logout.php">
                                <span class="glyphicon glyphicon-log-out" style="font-size: 25px;"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav">
                            <li><a  href="dashboard.php">Dashboard</a></li>
                            <li><a href="users.php">Users</a></li>
                            <li><a class="menu-top-active" href="posts.php">Posts</a></li>
                            <li><a href="comments.php">Comments</a></li>
                            <li><a  href="category.php">Categories</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->
    <?php
     if ($do == 'Manage') {
        //manages page ...

         // Select all users and "adminStatus !=1" to avoid admin
        $stmt = $conn->prepare("SELECT * FROM posts ");
        $stmt->execute(); 
        $rows=$stmt->fetchAll(); //assign to variables

       /*start Manage managers Page*/ 
    ?>


    <div class="content-wrapper">
        <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Posts</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Post's Info 
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="postClass text-center table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>post conetnt</th>
                                            <th>Post title</th>
                                            <th>published date</th>
                                            <th>Category</th>                                            
                                            <th>Status</th>
                                            <th>post image</th>
                                            <th>Controls</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php  
                                    foreach ($rows as $row ) {
                                        echo "<tr>";
                                        echo "<td>".$row['pID']."</td>";
                                        echo "<td>".$row['pContent']."</td>";
                                        echo "<td>".$row['pTitle']."</td>";
                                        echo "<td>".$row['published_date']."</td>";
                                        echo "<td>".$row['category']."</td>";
                                        echo "<td>".$row['post_status']."</td>";
                                        echo "<td>";
                                        if(!empty($row['pImage'])){
                                            echo   "<img src='".$row['pImage']."'/>";
                                        } else{
                                            echo "No managers Image";
                                        }
                                        echo "</td>";
                                        echo "<td> <a href = 'posts.php?do=Edit&postID=".$row['pID']."' class='btn btn-primary'>Edit</a>
                                                   <a href = 'posts.php?do=Delete&postID=".$row['pID']."' class='btn btn-danger confirm'>Delete</a></td>";
                                        echo "</tr>";
                                    }?>                                        
                                    </tbody>
                                </table>
                                <a href="#" class="btn btn-block btn-default">Add New Post</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <?php    
        } elseif($do == 'add') {
            //add Page .... ?>
            <div class="back">
            <div class="home-header">
                <h1 class="text-center main-header">Add New Post</h1>
            </div>
            <div class="container">
                <form class="form-horizontal has-success" action="?do=Insert" method="POST" enctype="multipart/form-data">
                    <!-- start First name field -->
                    <div class="form-group form-group-lg">
                        <label class="col-sm-2 control-label">Post Title</label>
                        <div class="col-sm-10">
                            <input type="text" name="ptitle" class="form-control" autocomplete="off" id="inputSuccess4" aria-describedby="inputSuccess4Status" required />
                        </div>
                    </div>
                    <!-- start Last name field -->
                    <div class="form-group form-group-lg">
                        <label class="col-sm-2 control-label">Post Content</label>
                        <div class="col-sm-10">
                            <input type="text" name="pcontent" class="form-control" autocomplete="off" id="inputSuccess4" aria-describedby="inputSuccess4Status" required/>
                        </div>
                    </div>
                    <!-- start email field -->
                    <div class="form-group form-group-lg">
                        <label class="col-sm-2 control-label">published Date</label>
                        <div class="col-sm-10">
                            <input type="text" name="pdate" class="form-control"  autocomplete="off" id="inputSuccess4" aria-describedby="inputSuccess4Status" required/>
                        </div>
                    </div>
                    <!-- start password field -->
                    <div class="form-group form-group-lg">
                        <label class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                        <?php
                            $stmt = $conn->prepare("SELECT * FROM categories ");
                            $stmt->execute(); 
                            $rows=$stmt->fetchAll(); //assign to variables
                            echo '<select name="cat" class="form-control" style="margin: 0 0 10px 0px;height: initial;">';
                            foreach($rows as $frow){
                                
                                echo '<option value='.$frow['cat_title'].'>';
                                echo $frow['cat_title'];
                                echo '</option>';
                                
                            }
                            echo '</select>';
                        ?>
                        </div>
                    </div>
                    <!-- start birthdate field -->
                    <div class="form-group form-group-lg">
                        <label class="col-sm-2 control-label">post status</label>
                        <div class="col-sm-10">
                        <select name="status" class="form-control" style="margin: 0 0 10px 0px;height: initial;">
                            <option value="approved">approved</option>
                            <option value="disApproved">disApproved</option>
                            <option value="deleted">deleted</option>
                        </select>
                        </div>
                    </div>
                    <!-- start picture field -->
                    <!-- start picture field -->
                    <div class="form-group form-group-lg">
                        <label class="col-sm-2 control-label">Select  Photo</label>
                        <div class="col-sm-10">
                            <input type="file" name="image" id="image" accept="image/*" size="32" required/>
                        </div>
                    </div>

                    <!-- start submit field -->
                    <div class="form-group form-group-lg">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input type="submit" value="Save User Data" class="btn btn-success btn-block" />
                        </div>
                    </div>
                </form>
            </div>
            </div>

        <?php 
        } elseif ($do == "Insert") {
            // insert Page
            // check if Data Comes from the form or not 
            if($_SERVER['REQUEST_METHOD'] =='POST') {
            echo  " <div class ='back'> " ;  
            echo  "<div class='home-header'> <h1 class='text-center main-header'> Insert Page </h1></div> " ;
            echo  " <div class ='container'> " ;

            //upload variables ......
            $avatarName = $_FILES['image']['name'];
            $avatarSize = $_FILES['image']['size'];
            $avatarTmp = $_FILES['image']['tmp_name'];
            $avatarType = $_FILES['image']['type'];

            $avatarAllowedExtension = array("jpeg", "jpg", "png", "gif");

            //get Allowed Extensions .....
            $arr = explode(".", $avatarName);
            $avatarExtension = strtolower(end($arr));

            // get variable from the form 
            $_ptitle   = $_POST['ptitle'] ;
            $_pcontent   = $_POST['pcontent'];
            $_pdate = $_POST['pdate'];
            $_pcat = $_POST['cat'];               
            $_pstatus    = $_POST['status'] ;
            
            //validate the form before Database
            $FormErrors = array();

            if (empty($_ptitle))    {  $FormErrors[] = "Please Insert title"; }
            if (empty($_pcontent))     {  $FormErrors[] = "Please Insert  content ";  }
            if (empty($_pdate))    {  $FormErrors[] = "Please Insert date"; }
            if (empty($_pcat))    {  $FormErrors[] = "Please Insert category"; }
            if (empty($_pstatus))    {  $FormErrors[] = "Please Insert status"; }
            if ( ! empty($avatarName) && ! in_array($avatarExtension,$avatarAllowedExtension)) {
                $FormErrors[] = " This Extension is not <strong> Allowed </strong> ";
            }
            if (empty($avatarName)) {
                $FormErrors[] = " The Image is <strong>Required</strong> ";
            }
            if ($avatarSize > 4194304 ) {
                $FormErrors[] = " The Image Size can not be larger than <strong>4MB</strong> ";
            }
                
            // loop into Error Array and echo it 
            foreach ($FormErrors as $error) {
                echo "<div class= 'alert alert-danger'>". $error ."</div>" ; 
            }
                                            
            //if there's no error in array then Update the DataBase 
            if (empty($FormErrors)) {

                //for uploading image ....
                //$avatar = rand(0, 10000000) . '_' . $avatarName;
                move_uploaded_file($avatarTmp,$avatarName);

                //Check If users Exist in Database or not
                $value= "$_email";
                $check = checkitem("email","users",$value) ;

                if ($check==1){

                    $errorMsg = "<div class='alert alert-danger'> This User Already Exist</div> ";
                    RedirctHome($errorMsg, 5);

                } else {


                  //define("UPLOAD_DIR", "/var/www/html/AdminPanel/img/");

                   if (!empty($_FILES["image"])) {
                       $myFile = $_FILES["image"];
                    
                       /*if ($myFile["error"] !== UPLOAD_ERR_OK) {
                           echo "<p>An error occurred.</p>";
                           exit;
                       }*/

                       // ensure a safe filename
                       //$pname = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

                       // don't overwrite an existing file
                       /*$i = 0;
                       $parts = pathinfo($pname);
                       while (file_exists(UPLOAD_DIR . $pname)) {
                           $i++;
                           $pname = $parts["filename"] . "-" . $i . "." . $parts["extension"];
                       }*/
                        
                       // preserve file from temporary directory
                       //$success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $pname);

                       /*
                       if (!$success) {
                           echo "<p>Unable to save file.</p>";
                           exit;
                       }*/

                       // set proper permissions on the new file
                       //chmod(UPLOAD_DIR . $pname, 0644);
                       //$path="assets/img/".$pname;

                    $stmt =$conn->prepare("INSERT INTO 
                                                posts(pTitle,pContent,published_date,category,pImage,post_status)
                                     VALUES     (:za,:zb,:zc,:zd,:ze,:zf)");
               
                    $stmt->execute(array(  
                                     ':za' => $_ptitle ,
                                     ':zb' => $_pcontent ,
                                     ':zc' => $_pdate ,
                                     ':zd' => $_pcat ,                                     
                                    ':zf' => $avatarName,
                                    ':ze' => $_pstatus 
                                       ));

                     $errorMsg = "<div class='alert alert-success'>" . $stmt->rowcount() . ' Record Added</div> ';
                     RedirctHome($errorMsg, 5);

                 }
                }
               //Notice: take care of order of variables  
               // insert into database   
                } //end of execution of Database

                  echo "</div>";
                  echo "</div>";

             } else{ $errorMsg=' You cannot access this page directly '; RedirctHome($errorMsg,5); } // if Data NOT!Comes from the form


            } 
            elseif ($do == 'Edit'){
            /* Edit Page */  
            $postID = isset($_GET['postID']) && is_numeric($_GET['postID']) ? intval($_GET['postID']) : 0 ;
            $stmt = $conn->prepare(" SELECT * FROM  posts WHERE pID =? LIMIT 1 "); // to fetch data from db to form to edit 
            $stmt->execute(array($postID));
            $row = $stmt->fetch();

            // check if id exist in database or not 
            if ($stmt->rowcount() > 0 ) { ?>
                 <div class="back" style="height: 135%;">
                 <div class="container">
                   <div class="col-md-push-3 col-md-6 col-sm-push-3 col-sm-6 col-xs-12">
                        <div class="profile-info text-center clearfix" style="float: initial;">
                        <div class="home-header">    
                            <h1 class="text-center main-header"> Edit Page </h1>    
                        </div>
                            <div class="profile-contents">

                                <form action="?do=Update" method="POST" enctype="multipart/form-data">
                                    
                                    <div class="form-group has-success">
                                        
                                        <input type="hidden" name="id"  
                                        value="<?php echo $postID ?>" />

                                        <input type="text" name="editFname" 
                                        class="form-control" placeholder="First Name" 
                                        id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="<?php echo $row['fname']?>" required />

                                        <input type="text" name="editLname" 
                                        class="form-control" placeholder="Last Name" 
                                        id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="<?php echo $row['lname']?>" required />

                                        <input type="text" name="editUsername" 
                                        class="form-control" placeholder="UseName" 
                                        id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="<?php echo $row['username']?>" required />

                                        <input type="email" name="editemail"  autocomplete="off"
                                        class="form-control" placeholder="User Email" id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="<?php echo $row['email']?>" required="required" />

                                        <input type="hidden" name="oldpass"  
                                         value="<?php echo $row['password']?>">

                                        <input type="password" name="newpass" autocomplete="New-Password" 
                                        class="form-control" placeholder="Change Password"
                                        id="inputSuccess4" aria-describedby="inputSuccess4Status" />
                                    <?php
                                        if($row['gender'] == 'mail'){?>
                                        <input type="radio" name="editGender" 
                                        class="" id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="male" checked/>Male
                                        
                                        <input type="radio" name="editGender" 
                                        class="" id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="female"/>Female
                                        <?php
                                        }else{?>
                                        <input type="radio" name="editGender" 
                                        class="" id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="male"/>Male

                                        <input type="radio" name="editGender" 
                                        class="" id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="female" checked/>Female
                                        <?php
                                        }
                                        ?>
                                        <input type="text" name="editBirth" 
                                        class="form-control" placeholder="Enter your Birthdate" 
                                        id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="<?php echo $row['birthdate']?>" />

                                        <input type="text" name="editCountry" 
                                        class="form-control" placeholder="Enter Country" 
                                        id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                        value="<?php echo $row['country']?>" />

                                        <input type="file" name="img" id="image" accept="image/*"
                                               onchange="preview_image(event)" value="<?php echo $row['userPhoto']?>" size="32"/>

                                        <img id="output_image" src="assets/img/<?php echo $row['userPhoto']?>"/>    
                                    </div>

                                    <div class="save-button">
                                        <button class="btn btn-success btn-block"  name="insert" > Save Changes </button> 
                                    </div> 
                                </form> 
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>

                    <?php

                    } else {echo "No ID Such that ";}
                        
                    } elseif ($do == 'Update') {
                        //update page 
                       echo  " <div class ='back'> " ;  
                       echo  "<div class='home-header'> <h2 class='text-center main-header'> Update  Page </h2> </div>" ;
                       echo  " <div class ='container'> " ; 

                       if($_SERVER['REQUEST_METHOD'] == 'POST') {

                           //upload variables ......
                           $EavatarName = $_FILES['img']['name'];
                           $EavatarSize = $_FILES['img']['size'];
                           $EavatarTmp = $_FILES['img']['tmp_name'];
                           $EavatarType = $_FILES['img']['type'];

                           $EavatarAllowedExtension = array("jpeg", "jpg", "png", "gif");

                           //get Allowed Extensions .....
                           $arr = explode(".", $EavatarName);
                           $EavatarExtension = strtolower(end($arr));

                           // get variable from the form

                           $_editID = $_POST['id'];
                           $_editFname = $_POST['editFname'];
                           $_editLname = $_POST['editLname'];
                           $_editUsername = $_POST['editUsername'];
                           $_editemail = $_POST['editemail'];
                           $_editGender = $_POST['editGender'];
                           //Password Trick
                           $_editPass = empty($_POST['newpass']) ? $_POST['oldpass'] : sha1($_POST['newpass']);

                           $_editBirth = $_POST['editBirth'];
                           $_editCountry = $_POST['editCountry'];

                           //validate the form before Database

                           $FormErrors = array();

                           if (strlen($_editemail) < 4 || strlen($_editemail) > 40) {

                               $FormErrors[] = "Please Make Your Email More Than 4 character and Less Than 40 character ";
                           }

                           if (empty($_editemail)) {

                               $FormErrors[] = "Please Insert Your Email";

                           }
                           if (empty($_editFname)) {

                               $FormErrors[] = "Please Insert Your First Name";
                           }
                           if (empty($_editPass)) {

                               $FormErrors[] = "Please Insert Your Password ";
                           }
                           if ( ! empty($EavatarName) && ! in_array($EavatarExtension,$EavatarAllowedExtension)) {
                               $FormErrors[] = " This Extension is not <strong> Allowed </strong> ";
                           }
                           if ($EavatarSize > 4194304 ) {
                               $FormErrors[] = " The Image Size can not be larger than <strong>4MB</strong> ";
                           }

                           // loop into Error Array and echo it
                           foreach ($FormErrors as $error) {
                               echo "<div class= 'alert alert-danger'>" . $error . "</div>";

                           }

                           //check if there's no error then Update the DataBase
                           if (empty($FormErrors)) {

                               //Update The Database with this info

                               //for uploading image ....
                               $Eavatar =  $EavatarName;

                               move_uploaded_file($EavatarTmp, $Eavatar);

//                               //update pic
//                               define("UPLOAD_DIR", "/var/htdocs/Association.with.CMS/Admin/layout/images/members/");
//
//
//                               if (!empty($_FILES["image"])) {
//
//                                   /** @var TYPE_NAME $myFile */
//                                   $myFile = $_FILES["image"];
//
//                                   if ($myFile["error"] !== UPLOAD_ERR_OK) {
//
//                                       $stmt = $conn->prepare(" UPDATE managers
//
//                                                SET fullname=:za , email=:zb ,password=:zc,
//                                                    work=:zd , address=:ze , phonenumber=:zf
//                                                WHERE id=:zg  ");
//
//                                       $stmt->execute(array(':za' => $_editFullname,
//                                           ':zb' => $_editemail,
//                                           ':zc' => $_hashedPass,
//                                           ':zd' => $_editWork,
//                                           ':ze' => $_editAddress,
//                                           ':zf' => $_editPhone,
//                                           ':zg' => $_editID
//                                       ));
//
//                                       echo "<div class='alert alert-success'>" . $stmt->rowcount() .
//                                           'Record Updated</div>';
//
//                                   } else {
//
//                                       // ensure a safe filename
//                                       $pname = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
//
//                                       // don't overwrite an existing file
//                                       $i = 0;
//
//                                       $parts = pathinfo($pname);
//                                       while (file_exists(UPLOAD_DIR . $pname)) {
//                                           $i++;
//                                           $pname = $parts["filename"] . "-" . $i . "." . $parts["extension"];
//                                       }
//
//                                       // preserve file from temporary directory
//                                       $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $pname);
//
//
//                                       if (!$success) {
//                                           echo "<p>Unable to save file.</p>";
//                                           exit;
//                                       }
//
//                                       // set proper permissions on the new file
//                                       chmod(UPLOAD_DIR . $pname, 0644);
//                                       $path = "../layout/images/members/" . $pname;

                                       //Notice: take care of order of variables

                                       $stmt = $conn->prepare(" UPDATE users
                                                                SET fname=:za , lname=:zb ,username=:zc,
                                                                    email=:zd , password=:ze , gender=:zf , birthdate=:zg,
                                                                    country=:zh, status='active' , adminStatus=0, userPhoto=:zi
                                                                WHERE id=:zj  ");

                                       $stmt->execute(array(
                                           ':za' => $_editFname,
                                           ':zb' => $_editLname,
                                           ':zc' => $_editUsername,
                                           ':zd' => $_editemail,
                                           ':ze' => $_editPass,
                                           ':zf' => $_editGender,
                                           ':zg' => $_editBirth,
                                           ':zh' => $_editCountry,
                                           ':zi' => $Eavatar,
                                           ':zj' => $_editID
                                       ));

                                       $errorMsg = "<div class='alert alert-success'>" . $stmt->rowcount() .  ' Record Updated</div> ';
                                       RedirctHome($errorMsg, 4);
                                   }
                                   echo "</div>";
                                   echo "</div>";
                               } else {
                                   $errorMsg = " You can't access this page directly ";
                                   RedirctHome($errorMsg, 3);
                               }

                   } elseif ($do == 'Delete') {

                        //Delete Page 

                    echo  " <div class ='back'> " ;
                    echo  "<div class='home-header'> <h2 class='text-center main-header'> Delete  Page </h2> </div>" ;
                    echo  " <div class ='container'> " ;

                        $postID = isset($_GET['postID']) && is_numeric($_GET['postID']) ? intval($_GET['postID']) : 0 ;
                        $stmt = $conn->prepare(" SELECT * FROM  posts WHERE pID =? LIMIT 1 "); 
                        $stmt->execute(array($postID));

                        // check if id exist in database or not 
                        if ($stmt->rowcount() > 0 ) { 
                            
                            $stmt=$conn->prepare("DELETE FROM posts WHERE pID=:zg ");
                            $stmt->bindParam(":zg",$postID);
                            $stmt->execute();

                            $errorMsg = "<div class='alert alert-success'>". $stmt->rowcount() . ' Record Deleted </div> ' ;
                            RedirctHome($errorMsg, 4);
                        } else {
                            echo "This ID not exist "; 
                        }
                    echo "</div>";
                    echo "</div>";    
                    ?>

    <?php 
        }
    require_once 'footer.php'; 

    } else{
        header("Location: ../login.php");
    }


?>