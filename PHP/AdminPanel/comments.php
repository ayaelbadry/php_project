<?php
    require_once 'header.php';

    if (isset($_SESSION['adminStatus']) == 0) {

        $do = isset($_GET['do']) ? $_GET['do'] : 'Manage';

?>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="left-div">
                <div class="user-settings-wrapper">
                    <ul class="nav">
                       <li style="text-align: left; float: left; font-size: 32px; margin-top: 15px;">Socail Media </li>
                        <li>
                            <a href="homePage.php">
                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                            </a>
                        </li>
                       <li>
                            <a href="logout.php">
                                <span class="glyphicon glyphicon-log-out" style="font-size: 25px;"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav">
                            <li><a  href="dashboard.php">Dashboard</a></li>
                            <li><a href="users.php">Users</a></li>
                            <li><a href="posts.php">Posts</a></li>
                             <li><a class="menu-top-active" href="comments.php">Comments</a></li>
                            <li><a href="category.php">Categories</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->

    <!-- CONTENT-WRAPPER SECTION END-->
    <?php
     if ($do == 'Manage') {
        //manages page ...
         // Select all users and "adminStatus !=1" to avoid admin
        $stmt = $conn->prepare("SELECT * FROM comments ");
        $stmt->execute(); 
        $rows=$stmt->fetchAll(); //assign to variables

       /*start Manage managers Page*/ 
    ?>

    <div class="content-wrapper">
        <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Comments</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Comments
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="comClass text-center table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>Content</th>
                                            <th>comment_date</th>                                            
                                            <th>Status</th>
                                            <th>Controls</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php  
                                        foreach ($rows as $row ) {
                                            echo "<tr>";
                                            echo "<td>".$row['CID']."</td>";
                                            echo "<td>".$row['comm_content']."</td>";
                                            echo "<td>".$row['comm_date']."</td>";
                                            echo "<td>".$row['comm_status']."</td>";                                            
                                            echo "<td> <a href = 'comments.php?do=Edit&commID=".$row['CID']."' class='btn btn-primary'>Edit</a>
                                                    <a href = 'comments.php?do=Delete&commID=".$row['CID']."' class='btn btn-danger confirm'>Delete</a></td>";
                                            echo "</tr>";
                                        }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                     <!-- End  Kitchen Sink -->
                </div>
            </div>
        </div>
    </div>

<?php    
    } elseif ($do == 'Edit'){
        /* Edit Page */  
        $commID = isset($_GET['commID']) && is_numeric($_GET['commID']) ? intval($_GET['commID']) : 0 ;
        $stmt = $conn->prepare("SELECT * FROM comments WHERE CID =? LIMIT 1 "); // to fetch data from db to form to edit 
        $stmt->execute(array($commID));
        $row = $stmt->fetch();
        
        // check if id exist in database or not 
        if ($stmt->rowcount() > 0 ) { ?>
                <div class="back" style="height: 135%;">
                <div class="container">
                <div class="col-md-push-3 col-md-6 col-sm-push-3 col-sm-6 col-xs-12">
                    <div class="profile-info text-center clearfix" style="float: initial;">
                    <div class="home-header">    
                        <h1 class="text-center main-header"> Edit Page </h1>    
                    </div>
                        <div class="profile-contents">
                            <form action="?do=Update" method="POST" enctype="multipart/form-data">
                                
                                <div class="form-group has-success">
                                    
                                    <input type="hidden" name="CID"  
                                    value="<?php echo $commID ?>" />

                                    <input type="text" name="editContent" 
                                    class="form-control" placeholder="edit content" 
                                    id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                    value="<?php echo $row['comm_content']?>" required />

                                    <input type="text" name="editDate" 
                                    class="form-control" placeholder="add Category date" 
                                    id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                    value="<?php echo $row['comm_date']?>" required />

                                    <input type="text" name="editStatus" 
                                    class="form-control" placeholder="add Category status" 
                                    id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                    value="<?php echo $row['comm_status']?>" required />

                                </div>

                                <div class="save-button">
                                    <button class="btn btn-success btn-block"  name="insert" > Save Changes </button> 
                                </div> 
                            </form> 
                        </div>
                    </div>
                    </div>
                    </div>
                </div>

                <?php

                } else {echo "No ID Such that ";}    
        } 
                elseif ($do == 'Update') {
                    //update page 
                    echo  " <div class ='back'> " ;  
                    echo  "<div class='home-header'> <h2 class='text-center main-header'> Update  Page </h2> </div>" ;
                    echo  " <div class ='container'> " ; 

                    if($_SERVER['REQUEST_METHOD'] == 'POST') {

                        // get variable from the form

                        $_editID = $_POST['CID'];
                        $_editContent = $_POST['editContent'];
                        $_editDate = $_POST['editDate'];
                        $_editStatus = $_POST['editStatus'];

                        //validate the form before Database

                        $FormErrors = array();

                        if (empty($_editContent)) {
                            $FormErrors[] = "Please Insert Category content";
                        }
                        if (empty($_editDate)) {
                            $FormErrors[] = "Please Insert Category date";
                        }
                        if (empty($_editStatus)) {
                            $FormErrors[] = "Please Insert Category status";
                        }

                        // loop into Error Array and echo it
                        foreach ($FormErrors as $error) {
                            echo "<div class= 'alert alert-danger'>" . $error . "</div>";
                        }

                        //check if there's no error then Update the DataBase
                        if (empty($FormErrors)){
                                    //Notice: take care of order of variables
                                    $stmt = $conn->prepare(" UPDATE comments
                                                            SET comm_content=:za, comm_date=:zb,
                                                            comm_status=:zc
                                                            WHERE CID=:zd ");

                                    $stmt->execute(array(
                                        ':za' => $_editContent,
                                        ':zb' => $_editDate,
                                        ':zc' => $_editStatus,                                        
                                        ':zd' => $_editID
                                    ));

                                    $errorMsg = "<div class='alert alert-success'>" . $stmt->rowcount() .  ' Record Updated</div> ';
                                    RedirctHome($errorMsg, 5);
                                }
                                echo "</div>";
                                echo "</div>";
                            } else {
                                $errorMsg = " You can't access this page directly ";
                                RedirctHome($errorMsg, 5);
                            }

                } elseif ($do == 'Delete') {
                    //Delete Page 
                echo  " <div class ='back'> " ;
                echo  "<div class='home-header'> <h2 class='text-center main-header'> Delete  Page </h2> </div>" ;
                echo  " <div class ='container'> " ;

                    $commID = isset($_GET['commID']) && is_numeric($_GET['commID']) ? intval($_GET['commID']) : 0 ;
                    $stmt = $conn->prepare("SELECT * FROM  comments WHERE CID =? LIMIT 1 "); 
                    $stmt->execute(array($commID));
                    
                    // check if id exist in database or not 
                    if ($stmt->rowcount() > 0 ) { 
                        
                        $stmt=$conn->prepare("DELETE FROM comments WHERE CID=:zg ");
                        $stmt->bindParam(":zg",$commID);
                        $stmt->execute();

                        $errorMsg = "<div class='alert alert-success'>". $stmt->rowcount() . ' Record Deleted </div> ' ;
                        RedirctHome($errorMsg, 8);
                    } else {
                        echo "This ID not exist "; 
                    }
                echo "</div>";
                echo "</div>";    
                ?>                                     
<?php 
     } 
    require_once 'footer.php'; 

    } else{
        header("Location: ../login.php");
    }


?>
