<?php

/* 
**  Redirct Function [This Function Accept Parameter]
**  $theMsg = Echo The Error/Success  Message 
**  $seconds=  Seconds Before Redircting to any Page   
**
*/

function getTitle() {
    global $pageTitle;

    if(isset($pageTitle)){
        echo $pageTitle;
    } else{
        echo 'Default';
    }
}


function RedirctHome($theMsg,$seconds=3){
	echo "$theMsg";
	echo "<div class='alert alert-info'>You Will be Redirected To the Home Page After $seconds seconds. </div>";
	$url= $_SERVER['HTTP_REFERER'];

     header("Cache-Control: no-cache");
     header("Pragma: no-cache");
	 header("refresh: $seconds; url = '$url' ");
	 exit();
}


/* 
**  Check Item In DataBase [Function Accept Parameters]
**  $select = Item that Selected [Example: username ,itemname , Categoryname ]
**  $from = the table to select from [Example : users ]
** $value = value of select 
*/

function checkitem($select,$from,$value){

	global $conn ;
	$stmt1 =$conn-> prepare("SELECT $select FROM $from WHERE $select= ? ");
	$stmt1 ->execute(array($value)); 
	$count= $stmt1 ->rowCount();
	return $count;

}

function countMEM($MEM, $table){

    global $conn;

    $stmt2 = $conn->prepare("SELECT COUNT($MEM) FROM $table WHERE adminStatus != 1 AND status = 'deactive'");
    $stmt2->execute();

    return $stmt2->fetchColumn();
}

function countPos($MEM, $table){

    global $conn;

    $stmt4 = $conn->prepare("SELECT COUNT($MEM) FROM $table WHERE post_status = 'disapproved'");
    $stmt4->execute();

    return $stmt4->fetchColumn();
}

function countComm($MEM, $table){

    global $conn;

    $stmt4 = $conn->prepare("SELECT COUNT($MEM) FROM $table WHERE comm_status = 'draft'");
    $stmt4->execute();

    return $stmt4->fetchColumn();
}

function countMess($MEM, $table){

    global $conn;

    $stmt3 = $conn->prepare("SELECT COUNT($MEM) FROM $table");
    $stmt3->execute();

    return $stmt3->fetchColumn();
}

?> 