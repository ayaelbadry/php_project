
<?php
    ob_start();
    session_start();
    require_once 'connection.php';
    require_once 'Functions.php';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>ٍSocail Media Admin</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/adminStyle.css" rel="stylesheet" />
     <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .btn{
            /*padding: 7px 20px;*/
            border-radius: 7px;
        }
        .comClass td:nth-child(2){
            width: 50%;
        }
        .postClass td:nth-child(2){
            width: 40%;
        }
        .usersClass td:nth-child(6){
            width: 15%;
        }
        .back{
            padding: 40px 0 60px;
        }
        .main-header{
            margin: 10px 0 40px;
        }
    </style>
</head>
<body>
    <!-- HEADER END-->