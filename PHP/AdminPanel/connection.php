<?php

$servername = "localhost";
$username = "root";
$password = "0000";
$mysql_database = 'socialmedia';

$option= array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
);

try {
    $conn = new PDO("mysql:host=$servername;dbname=$mysql_database;charset=utf8", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
}

catch(PDOException $e) {

    echo "Failed To connect ".$e->getMessage();
}

?>