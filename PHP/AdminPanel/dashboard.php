<?php
    require_once 'header.php';

    if (isset($_SESSION['adminStatus']) == 0) {
?>

    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="left-div">
                <div class="user-settings-wrapper">
                    <ul class="nav">
                       <li style="text-align: left; float: left; font-size: 32px; margin-top: 15px;">Socail Media </li>
                        <li>
                            <a href="homePage.php">
                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                            </a>
                        </li>
                       <li>
                            <a href="logout.php">
                                <span class="glyphicon glyphicon-log-out" style="font-size: 25px;"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav">
                            <li><a class="menu-top-active" href="dashboard.php">Dashboard</a></li>
                            <li><a href="users.php">Users</a></li>
                            <li><a href="posts.php">Posts</a></li>
                             <li><a href="comments.php">Comments</a></li>
                            <li><a href="category.php">Categories</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Dashboard</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6">
                   <a href="users.php?do=Manage&page=Pending" style="text-decoration: none;">
                    <div class="dashboard-div-wrapper bk-clr-one">
                        <i  class="fa fa-users dashboard-div-icon" ></i>
                        <div class="progress progress-striped active">
                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            </div>   
                        </div>
                         <h4>Users wait for Activating <?php echo countMEM('id','users') ;?>  </h4>
                    </div>
                    </a>
                </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                   <a href="posts.php?do=Manage&page=Pending" style="text-decoration: none;">
                    <div class="dashboard-div-wrapper bk-clr-two">
                        <i  class="fa fa-edit dashboard-div-icon" ></i>
                        <div class="progress progress-striped active">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                          </div> 
                        </div>
                         <h4>Posts wait for Activating <?php echo countPos('pID','posts') ;?> </h4>
                    </div>
                    </a>
                </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                   <a href="comments.php?do=Manage&page=Pending" style="text-decoration: none;">
                    <div class="dashboard-div-wrapper bk-clr-three">
                        <i  class="fa fa-comments dashboard-div-icon" ></i>
                        <div class="progress progress-striped active">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                          </div>                           
                        </div>
                         <h4>Comments wait for Activating <?php echo countComm('CID','comments') ;?></h4>
                    </div>
                     </a>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                   <a href="category.php" style="text-decoration: none;">
                    <div class="dashboard-div-wrapper bk-clr-four">
                        <i  class="fa fa-cogs dashboard-div-icon" ></i>
                        <div class="progress progress-striped active">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                            </div>       
                        </div>
                         <h4>Manage Categories </h4>
                    </div>
                    </a>
                </div>
            </div>
            <div class="row">
                
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->

<?php 

require_once 'footer.php'; 

    } else{
        header("Location: ../login.php");
    }


?>
