﻿<?php
    require_once 'header.php';

    if (isset($_SESSION['adminStatus']) == 0) {

        $do = isset($_GET['do']) ? $_GET['do'] : 'Manage';
?>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="left-div">
                <div class="user-settings-wrapper">
                    <ul class="nav">
                       <li style="text-align: left; float: left; font-size: 32px; margin-top: 15px;">Socail Media </li>
                        <li>
                            <a href="homePage.php">
                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                            </a>
                        </li>
                       <li>
                            <a href="logout.php">
                                <span class="glyphicon glyphicon-log-out" style="font-size: 25px;"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav">
                            <li><a  href="dashboard.php">Dashboard</a></li>
                            <li><a href="users.php">Users</a></li>
                            <li><a href="posts.php">Posts</a></li>
                             <li><a href="comments.php">Comments</a></li>
                            <li><a class="menu-top-active" href="category.php">Categories</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->
    <?php
     if ($do == 'Manage') {
        //manages page ...
         // Select all users and "adminStatus !=1" to avoid admin
        $stmt = $conn->prepare("SELECT * FROM categories ");
        $stmt->execute(); 
        $rows=$stmt->fetchAll(); //assign to variables

       /*start Manage managers Page*/ 
    ?>


    <div class="content-wrapper">
        <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Categories</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Post's Categories
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="text-center table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>Cateory Name</th>
                                            <th>Controls</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  
                                        foreach ($rows as $row ) {
                                            echo "<tr>";
                                            echo "<td>".$row['cat_id']."</td>";
                                            echo "<td>".$row['cat_title']."</td>";
                                            echo "<td> <a href = 'category.php?do=Edit&catID=".$row['cat_id']."' class='btn btn-primary'>Edit</a>
                                                    <a href = 'category.php?do=Delete&catID=".$row['cat_id']."' class='btn btn-danger confirm'>Delete</a></td>";
                                            echo "</tr>";
                                        }?>
                                        <tr>
                                            <td colspan="3">
                                                <a href="category.php?do=add" class="btn btn-block btn-default">Add New Category</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                     <!-- End  Kitchen Sink -->
                </div>
            </div>
        </div>
    </div>
    
    <!-- CONTENT-WRAPPER SECTION END-->

<?php    
    } elseif($do == 'add') {
        //add Page .... ?>
        <div class="back">
        <div class="home-header">
            <h1 class="text-center main-header">Add New Category</h1>
        </div>
        <div class="container">
            <form class="form-horizontal has-success" action="?do=Insert" method="POST" enctype="multipart/form-data">
                <!-- start First name field -->
                <div class="form-group form-group-lg">
                    <label class="col-sm-2 control-label">Category Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="cname" class="form-control" autocomplete="off" id="inputSuccess4" aria-describedby="inputSuccess4Status" required />
                    </div>
                </div>
                <!-- start submit field -->
                <div class="form-group form-group-lg">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="submit" value="Save Data" class="btn btn-success btn-block" />
                    </div>
                </div>
            </form>
        </div>
        </div>

    <?php 
    } elseif ($do == "Insert") {
        // insert Page
        // check if Data Comes from the form or not 
        if($_SERVER['REQUEST_METHOD'] =='POST') {
        echo  " <div class ='back'> " ;  
        echo  "<div class='home-header'> <h1 class='text-center main-header'> Insert Page </h1></div> " ;
        echo  " <div class ='container'> " ;

        // get variable from the form 
        $_catName   = $_POST['cname'] ;
        
        //validate the form before Database
        $FormErrors = array();
        if (empty($_catName))    {  $FormErrors[] = "Please Insert category Name"; }
        // loop into Error Array and echo it 
        foreach ($FormErrors as $error) {
            echo "<div class= 'alert alert-danger'>". $error ."</div>" ; 
        }
                                        
        //if there's no error in array then Update the DataBase 
        if (empty($FormErrors)) {

                $stmt =$conn->prepare("INSERT INTO 
                                            categories(cat_title)
                                    VALUES (:ztitle)");
            
                $stmt->execute(array(  ':ztitle' => $_catName));

                $errorMsg = "<div class='alert alert-success'>" . $stmt->rowcount() . ' Record Added</div> ';
                RedirctHome($errorMsg, 8);

                }
            //Notice: take care of order of variables  
            // insert into database   
            //end of execution of Database

                echo "</div>";
                echo "</div>";

        }
    }
        elseif ($do == 'Edit'){
        /* Edit Page */  
        $catID = isset($_GET['catID']) && is_numeric($_GET['catID']) ? intval($_GET['catID']) : 0 ;
        $stmt = $conn->prepare("SELECT * FROM categories WHERE cat_id =? LIMIT 1 "); // to fetch data from db to form to edit 
        $stmt->execute(array($catID));
        $row = $stmt->fetch();
        
        // check if id exist in database or not 
        if ($stmt->rowcount() > 0 ) { ?>
                <div class="back" style="height: 135%;">
                <div class="container">
                <div class="col-md-push-3 col-md-6 col-sm-push-3 col-sm-6 col-xs-12">
                    <div class="profile-info text-center clearfix" style="float: initial;">
                    <div class="home-header">    
                        <h1 class="text-center main-header"> Edit Page </h1>    
                    </div>
                        <div class="profile-contents">
                            <form action="?do=Update" method="POST" enctype="multipart/form-data">
                                
                                <div class="form-group has-success">
                                    
                                    <input type="hidden" name="cat_id"  
                                    value="<?php echo $catID ?>" />

                                    <input type="text" name="editCat" 
                                    class="form-control" placeholder="add Category name" 
                                    id="inputSuccess4" aria-describedby="inputSuccess4Status"
                                    value="<?php echo $row['cat_title']?>" required />
                                </div>

                                <div class="save-button">
                                    <button class="btn btn-success btn-block"  name="insert" > Save Changes </button> 
                                </div> 
                            </form> 
                        </div>
                    </div>
                    </div>
                    </div>
                </div>

                <?php

                } else {echo "No ID Such that ";}    
        } 
                elseif ($do == 'Update') {
                    //update page 
                    echo  " <div class ='back'> " ;  
                    echo  "<div class='home-header'> <h2 class='text-center main-header'> Update  Page </h2> </div>" ;
                    echo  " <div class ='container'> " ; 

                    if($_SERVER['REQUEST_METHOD'] == 'POST') {

                        // get variable from the form

                        $_editID = $_POST['cat_id'];
                        $_editcatName = $_POST['editCat'];

                        //validate the form before Database

                        $FormErrors = array();

                        if (empty($_editcatName)) {
                            $FormErrors[] = "Please Insert Category name";
                        }

                        // loop into Error Array and echo it
                        foreach ($FormErrors as $error) {
                            echo "<div class= 'alert alert-danger'>" . $error . "</div>";
                        }

                        //check if there's no error then Update the DataBase
                        if (empty($FormErrors)){
                                    //Notice: take care of order of variables
                                    $stmt = $conn->prepare(" UPDATE categories
                                                            SET cat_title=:za
                                                            WHERE cat_id=:zj ");

                                    $stmt->execute(array(
                                        ':za' => $_editcatName,
                                        ':zj' => $_editID
                                    ));

                                    $errorMsg = "<div class='alert alert-success'>" . $stmt->rowcount() .  ' Record Updated</div> ';
                                    RedirctHome($errorMsg, 8);
                                }
                                echo "</div>";
                                echo "</div>";
                            } else {
                                $errorMsg = " You can't access this page directly ";
                                RedirctHome($errorMsg, 5);
                            }

                } elseif ($do == 'Delete') {
                    //Delete Page 
                echo  " <div class ='back'> " ;
                echo  "<div class='home-header'> <h2 class='text-center main-header'> Delete  Page </h2> </div>" ;
                echo  " <div class ='container'> " ;

                    $catID = isset($_GET['catID']) && is_numeric($_GET['catID']) ? intval($_GET['catID']) : 0 ;
                    $stmt = $conn->prepare("SELECT * FROM  categories WHERE cat_id =? LIMIT 1 "); 
                    $stmt->execute(array($catID));
                    
                    // check if id exist in database or not 
                    if ($stmt->rowcount() > 0 ) { 
                        
                        $stmt=$conn->prepare("DELETE FROM categories WHERE cat_id=:zg ");
                        $stmt->bindParam(":zg",$catID);
                        $stmt->execute();

                        $errorMsg = "<div class='alert alert-success'>". $stmt->rowcount() . ' Record Deleted </div> ' ;
                        RedirctHome($errorMsg, 8);
                    } else {
                        echo "This ID not exist "; 
                    }
                echo "</div>";
                echo "</div>";    
                ?>                                     
<?php 
     }
    require_once 'footer.php'; 

    } else{
        header("Location: ../login.php");
    }


?>