<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    &copy; 2018 Copyrights Reserved
                </div>
            </div>
        </div>
    </footer>
    <script>
        $('.confirm').click(function(){
            return confirm('Are You Sure ?!? ');
        });

        function preview_image(event)
        {
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>