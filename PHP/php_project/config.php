
<?php

require_once  __DIR__.'/Dependances/vendor/autoload.php';
require_once __DIR__.'/Dependances/vendor/php-activerecord/php-activerecord/ActiveRecord.php';

ob_start();
session_start();


$mainPath = __DIR__;
ActiveRecord\Config::initialize(function($cfg)
{
  $cfg->set_model_directory(__DIR__.'/models');
  $cfg->set_connections(array('development' =>
    "mysql://root:root@localhost/socialmedia"));
});

$loader = new Twig_Loader_Filesystem(__DIR__.'/templets');

$twig = new Twig_Environment($loader,array(

    //'cache' => __DIR__.'/Dependances/compilation_cach',
));

function render($path,$context){
  global $twig;
  return $twig->render($path,$context);
}

function hashPass($pass){
		if($pass){
			return sha1($pass);
		}
	}

function inputValidation($input){

  $input = trim($input);
  $input = stripslashes($input);
  $input = htmlspecialchars($input);
  return $input;

}
