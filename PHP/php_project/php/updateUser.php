<?php
require_once "../config.php";

$fname = "";
$lname ="";
$username = "";
$userRemember="";
$email="";
$password = "";
$repeat_pass = "";
$country = "";
$birth="";
$gender = "";
$image = "";
$error = array();
$old = array();


$userid=$_SESSION["currentUser"];
$user = Users::find_by_id($userid);

if(isset($_GET['fname'])  && !empty($_GET['fname'])){
  $fname = inputValidation($_GET['fname']);
  if(!preg_match("/^[a-zA-Z]+$/",$fname)){
    $error['fname']="Invalid Name";
  }
}

if(isset($_GET['lname'])  && !empty($_GET['lname'])){
  $lname = inputValidation($_GET['lname']);
  if(!preg_match("/^[a-zA-Z]+$/",$lname)){
    $error['lname']="Invalid Name";
  }
}

if(isset($_GET['username'])  && !empty($_GET['username'])){
  $username = inputValidation($_GET['username']);
  if(!preg_match("/^[a-zA-Z]+([0-9]|[a-zA-Z])*$/",$username)){
    $error['username']="user name must be letters and numbers only";
  }
}


if(isset($_GET['pass'])  && !empty($_GET['pass'])){
  $password = inputValidation($_GET['pass']);
  if( strlen($password) < 8
    || (!preg_match("#[0-9]+#",$password))
    || (!preg_match("#[a-zA-Z]+#",$password))
  ){
    $error['password']="Password must be numbers and letters and more than 8 characters uber and lower";
  }
}

if(isset($_GET['repeat_pass'])  && !empty($_GET['repeat_pass'])){
  $repeat_pass = inputValidation($_GET['repeat_pass']);
    if($repeat_pass !== $password){
      $error['password']=" Passwords is not matched ";
    }
}

if(isset($_GET["email"])&&!empty($_GET["email"])){
  $email = inputvalidation($_GET["email"]);
  if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $error["email"] ="Invalid email format";
  }
}

if(isset($_GET['country'])  && !empty($_GET['country'])){
  $username = inputValidation($_GET['country']);
  if(!preg_match("/^[a-zA-Z]+$/",$username)){
    $error['country']="Country in valid";

  }
}

if(isset($_GET['bdate'])  && !empty($_GET['bdate'])){
    $birth = $_GET['bdate'];
}

if(isset($_GET['gender'])  && !empty($_GET['gender'])){
    $gender = $_GET['gender'];
}

if(isset($_GET["uimage"])&&!empty($_GET["uimage"])){
  $image=$_GET["uimage"];
}

if(!$error){

  $user->fname = $fname;
   $user->lname = $lname;
   $user->username=$username;
   $user->email=$email;
   $user->password= hashPass($password);
   $user->gender = $gender;
   $user->birthdate = $birth;
   $user->country = $country;
   $user->save();

   header("location: home.php");
   die();
}

echo render('details.html',['error' => $error]);
die();
