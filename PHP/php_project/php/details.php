<?php
require_once "../config.php";

$userid=$_SESSION["currentUser"];
$user = Users::find_by_id($userid);
$error=[];
if(!$user->username){
  header("location: login.php");
}elseif($error){
    echo render('details.html',['error' => $error]);
    die();
}

$username=$user->username;
$password=$user->password;
$user = Users::find_by_username_and_password($username,$password);

echo render('details.html',array(
    'fname'=>$user->fname,
    'lname'=>$user->lname,
    'username'=>$user->username,
    'email'=>$user->email,
    'password'=>$user->password,
    'gender'=>$user->gender,
    'birthdate'=>$user->birthdate,
    'country'=>$user->country,
    //'userPhoto'=>$user->userPhoto
));
