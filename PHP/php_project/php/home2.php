<?php
require_once "../config.php";
$currentUser = "";
if(!isset($_SESSION["currentUser"]) || empty($_SESSION["currentUser"])){
  header('location: login.php');
  die();
}



$currentUser = $_SESSION["currentUser"];
$posts = Posts::find('all');
$users =[];

foreach($posts as $post){
      $user = Users::find($post->user_id);
      array_push ( $users , $user);
}


echo render('homePage.html',array(
      'posts' => $posts,
       'users' => $users,
      ));

//$now = $posts[0]->published_date;
//ActiveRecord\DateTime::$DEFAULT_FORMAT = 'short';
 //echo $now->format();         # 02 Jan 03:04
 //echo $now->format('atom');   # 2010-01-02T03:04:05-05:00
 //echo $now->format('Y-m-d');  # 2010-01-02
 # __toString() uses the default formatter
// echo (string)$now;           # 02 Jan 03:04
