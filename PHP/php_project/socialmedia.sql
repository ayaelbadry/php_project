-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 15, 2018 at 06:21 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `socialmedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(10) UNSIGNED NOT NULL,
  `cat_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(4, 'Art'),
(3, 'Edu'),
(5, 'food'),
(2, 'movies'),
(1, 'sport');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `CID` int(10) UNSIGNED NOT NULL,
  `comm_content` text NOT NULL,
  `comm_date` date NOT NULL,
  `comm_status` varchar(255) NOT NULL DEFAULT 'draft',
  `post_id` int(10) UNSIGNED NOT NULL,
  `comm_user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`CID`, `comm_content`, `comm_date`, `comm_status`, `post_id`, `comm_user_id`) VALUES
(1, 'the ingredints are eggs and flour', '2018-02-16', 'draft', 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `pID` int(11) UNSIGNED NOT NULL,
  `pTitle` varchar(255) NOT NULL,
  `pContent` text NOT NULL,
  `pComment` text NOT NULL,
  `published_date` date NOT NULL,
  `category` varchar(255) NOT NULL,
  `pImage` varchar(255) DEFAULT NULL,
  `post_status` varchar(255) NOT NULL DEFAULT 'disapproved',
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`pID`, `pTitle`, `pContent`, `pComment`, `published_date`, `category`, `pImage`, `post_status`, `user_id`) VALUES
(2, 'ITI', 'ITI is one of the largest educational organization in Egypt and also in the Middle East it aims to graduate qualified people in the market in the IT field in the way of learning students by doing not just teaching theoritical methods', '', '2018-02-20', 'Edu', NULL, 'disapproved', 1),
(3, 'World Cup', 'There will be a big match in its final stage between Egypt and moroco, playing on the world cup. The winner team will have the world cup for this season', '', '2018-02-05', 'sport', NULL, 'disapproved', 3),
(4, 'Cup Cakes', 'Cup cakes will be presented today by the best one who can introduce it. The chef is Doaa Mohamed and she will present the ingredints and the way to cook it ', '', '2018-02-14', 'food', NULL, 'disapproved', 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthdate` date NOT NULL,
  `country` varchar(100) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'deactive',
  `adminStatus` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `userPhoto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `username`, `email`, `password`, `gender`, `birthdate`, `country`, `status`, `adminStatus`, `userPhoto`) VALUES
(1, 'zinab', 'hassan', 'zozo', 'zozo@gmail.com', '00000000', '1', '2018-02-01', 'mans', 'deactive', 0, ''),
(3, 'Aya', 'ElBadry', 'aya_12', 'aya@gmail.com', '123', '1', '2017-12-04', 'Egypt', 'deactive', 0, ''),
(4, 'Abdulmenum', 'Mohamed', 'abelmuneum', 'abelmuneum@yahoo.com', '123', '0', '2018-02-01', 'Mans', 'deactive', 0, ''),
(5, 'Engy', 'Bakr', 'engy12', 'engy@yahoo.com', '123', '1', '2018-02-01', 'mans', 'deactive', 0, ''),
(6, 'Ebrahimm', 'Gamal', 'ebrahim', 'ebrahim@yahoo.com', '123', '0', '2017-08-22', 'Alex', 'deactive', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_title` (`cat_title`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`CID`),
  ADD KEY `comm_user_id` (`comm_user_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`pID`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `CID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `pID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`comm_user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `posts` (`pID`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`cat_title`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
