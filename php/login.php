<?php
require_once "../config.php";

$username = "";
$userpass="";
$userRemember="";
$error = "";


if(isset($_POST['username'])  && !empty($_POST['username'])){
  $username = inputValidation($_POST['username']);
  if(!preg_match("/^[a-zA-Z]+([0-9]|[a-zA-Z])*$/",$username)){
    $error = "user name must be letters and numbers only";
  }
}
if(isset($_POST['password'])  && !empty($_POST['password'])){
  $userpass = inputValidation($_POST['password']);
  if( strlen($userpass) < 8
    || (!preg_match("#[0-9]+#",$userpass))
    || (!preg_match("#[a-zA-Z]+#",$userpass))
  ){
    $error = "Password must be numbers and letters and more than 8 characters uber and lower";
  }
}

if(isset($_POST['rememberme']) && !empty($_POST['rememberme'])){
  //TODO Create Cookies and sessions
  $userRemember = $_POST['rememberme'];
}

//Book::all(array('conditions' => array('genre = ? AND price < ?', 'Romance', 15.00)));
if(!$username){
  if(isset($_COOKIE["username"]) && !empty($_COOKIE["username"])){
        if(isset($_COOKIE["password"]) && !empty($_COOKIE["password"])){
          //echo render('login.html',['username'=> $_COOKIE("username") , 'password'=> $_COOKIE("password")]);
          //$user = Users::all(array('conditions' => array('username = ? AND password = ?', $username, $userpass)));
            if(login($_COOKIE["username"],$_COOKIE["password"])){
                header('location: home.php');
            }else{
                header('location: login.php');
            }
            die();
        }
  }
  echo render('login.html',[]);
}else if($error){
    echo render('login.html',['error' => $error]);
}else if($username && $userpass){
//TODO redirect to home if valid
      if(login($username ,$userpass)){
          if($userRemember){
            setCookie("username",$username);
            setCookie("password",$userpass);
          }
            header('location: home.php');
            die();
      }else{
          $error = "Username and password dont match";
          echo render('login.html',['error' => $error]);
      }
  die();
}




function login($name, $pass){
    $user = Users::all(array('conditions' => array('username = ? AND password = ?', $name, $pass)));
    if($user){
      $_SESSION["currentUser"] =$user;
      return 1;
    }else{
      return 0;
    }
}
