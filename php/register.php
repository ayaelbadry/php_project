<?php
require_once '../config.php';

$fname = "";
$lname ="";
$username = "";
$userRemember="";
$email="";
$password = "";
$repeat_pass = "";
$country = "";
$birth="";
$gender = "";
$image = "";
$error = array();
$old = array();


if(isset($_GET['fname'])  && !empty($_GET['fname'])){
  $username = inputValidation($_GET['fname']);
  if(!preg_match("/^[a-zA-Z]+$/",$username)){
    $error['fname']="Invalid Name";
  }else{
    $newFname=$_GET["fname"];
  }
}

if(isset($_GET['lname'])  && !empty($_GET['lname'])){
  $username = inputValidation($_GET['lname']);
  if(!preg_match("/^[a-zA-Z]+$/",$username)){
    $error['lname']="Invalid Name";
  }
}

if(isset($_GET['username'])  && !empty($_GET['username'])){
  $username = inputValidation($_GET['username']);
  if(!preg_match("/^[a-zA-Z]+([0-9]|[a-zA-Z])*$/",$username)){
    $error['username']="user name must be letters and numbers only";
  }
}


if(isset($_GET['password'])  && !empty($_GET['password'])){
  $password = inputValidation($_GET['password']);
  if( strlen($password) < 8
    || (!preg_match("#[0-9]+#",$password))
    || (!preg_match("#[a-zA-Z]+#",$password))
  ){
    $error['password']="Password must be numbers and letters and more than 8 characters uber and lower";
  }
}

if(isset($_GET['repeat_pass'])  && !empty($_GET['repeat_pass'])){
  $repeat_pass = inputValidation($_GET['repeat_pass']);
    if($repeat_pass !== $password){
      $error['password']=" Passwords is not matched ";
    }
}

if(isset($_GET["email"])&&!empty($_GET["email"])){
  $email = inputvalidation($_GET["email"]);
  if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $error["email"] ="Invalid email format";
  }
}

if(isset($_GET['country'])  && !empty($_GET['country'])){
  $username = inputValidation($_GET['country']);
  if(!preg_match("/^[a-zA-Z]+$/",$username)){
    $error['country']="Country in valid";

  }
}

if(isset($_GET['birth'])  && !empty($_GET['birth'])){
    $birth = $_GET['birth'];
}

if(isset($_GET['gender'])  && !empty($_GET['gender'])){
    $gender = $_GET['gender'];
}

if(isset($_GET["uimage"])&&!empty($_GET["uimage"])){
  $image=$_GET["uimage"];
}



//Book::all(array('conditions' => array('genre = ? AND price < ?', 'Romance', 15.00)));
if(!$username){
  echo render('register.html',[]);
  die();
}else if($error){
    echo render('register.html',$error);
    die();
}else if($username && $password){

    $newUser = Users::all(array('conditions' => array('username = ?', $username)));
    if($newUser){
        $error["username"] = "username is used find another one";
        $newUser = Users::all(array('conditions' => array('email = ?' , $email)));
        if($newUser)
        $error['email']="email is used";
        echo render('register.html',$error);
        die();
    }



      $user = Users::create(array(
        'fname' => $fname,
       'lname' => $lname,
       'username'=>$username,
       'email'=>$email,
       'password'=> hashPass($password),
       'gender' => $gender,
       'birthdate' => $birth,
       'country' => $country,
       'status' => "active",
       'adminStatus' => 0,
       'userPhoto'=>""
     ));

     if($user){
       echo "user added";
     }



      /*if(login($username ,$userpass)){
          if($userRemember){
            setCookie("username",$username);
            setCookie("password",$userpass);
          }
            header('location: home.php');
            die();
      }else{
          $error = "Username and password dont match";
          echo render('login.html',['error' => $error]);
      }*/
  die();
}




function login($name, $pass){
    //$user = Users::all(array('conditions' => array('username = ? AND password = ?', $name, $pass)));
    // $attributes = array('title' => 'My first blog post!!', 'author_id' => 5);
    // $post = new Post($attributes);
    // $post->save();

    $attributes = array('fname' => $newFname , 'lname' => $newLname, 'username' => $newUsername, 'password' => $newPassword,
    'email' => $newEmail, 'country' => $newCountry, 'gander' => $newGander, 'status'=>'draft', 'userphoto' => $newImg );
     $user = new Users($attributes);
     $user->save();

    if($user){

      $_SESSION["currentUser"] =$user;
      return 1;
    }else{
      return 0;
    }
}




echo render('register.html',[]);
